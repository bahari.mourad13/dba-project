<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function() {
    Route::put('userLocation','QuestionController@saveLoggedUserLocation');
    Route::get('questions/add','QuestionController@create');
    Route::get('questions/favorites','QuestionController@displayFavorites');
    Route::post('questions/add','QuestionController@store');
    Route::post('questions/favorite','QuestionController@favorite');
    Route::post('questions/removeFavorite','QuestionController@removeFavorite');
    Route::post('questions/removeFavoriteIndex','QuestionController@removeFavoriteIndex');
    Route::get('questions','QuestionController@index');
    Route::get('questions/{id}',['as' => 'detail', 'uses' => 'ResponseController@index']);
    Route::post('responses/add','ResponseController@store');
});