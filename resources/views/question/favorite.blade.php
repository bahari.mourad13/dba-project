@extends('layouts.app')
@section('content')
    <div>
        <div class="row mr-5 ml-5 mt-5">
            <div class="btn btn-warning col-12 col-md-3 mb-3">
                <a class="btn btn-warning" href="{{action('QuestionController@index')}}">
                    <i class="fa fa-arrow-left mr-4"></i> Retour aux questions
                </a>
            </div>
        </div>
        <br/>
        <div class="row mr-5 ml-5 mt-2">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div><br/>
            @endif
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Utilisateur</th>
                    <th>Titre</th>
                    <th>Contenu</th>
                    <th>Location</th>
                    <th colspan="2">Action</th>
                </tr>
                </thead>
                <tbody>

                @foreach($questionsFavoris as $question)
                    <tr>
                        <td>{{$question->user->name}}</td>
                        <td>{{$question->title}}</td>
                        <td>{{$question->content}}</td>
                        <td>{{$question->location}}</td>
                        <td>
                            <a class="btn btn-outline-info  mr-1"
                               href="{{action('ResponseController@index', $question->id)}}"><i
                                        class="fa fa-eye"></i></a>
                            <a class="btn btn-danger text-white ml-1" onclick="event.preventDefault();
                                                     document.getElementById('{{$question->id}}').submit();"><i
                                        class="fa fa-heart"></i></a>
                            <form id="{{$question->id}}" action="{{url('questions/removeFavorite')}}" method="POST"
                                  style="display: none;">
                                @csrf
                                <input name="question_id" value="{{$question->id}}">
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection