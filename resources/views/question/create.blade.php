@extends('layouts.app')
@section('links')
    <style>
        #map {
            height: 100%;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
@endsection
@section('content')
    <div class="container mt-5 ml-5">
        <div class="btn btn-warning col-12 col-md-3 mb-3">
            <a class="btn btn-warning" href="{{action('QuestionController@index')}}">
                <i class="fa fa-arrow-left mr-4"></i> Retour aux questions
            </a>
        </div>
        <h5>Ajouter une question</h5><br/>
        <form method="post" action="{{url('questions/add')}}">
            @csrf
            <div class="row">
                <div class="form-group col-md-8 offset-4">
                    <label for="title">Titre:</label>
                    <input type="text" class="form-control" name="title" required>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-8 offset-4">
                    <label for="content">Contenu:</label>
                    <textarea type="text" class="form-control" name="content" required> </textarea>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-8 offset-4">
                    <div class="row" style="height: 500px">
                            <label for="content">Location:</label>
                            <input type="text" class="form-control mb-2" name="location" id="location" readonly>
                            <div id="map" style="width:100%;height:400px;"></div>
                            <input type="text" hidden name="lat" id="lat" >
                            <input type="text" hidden name="lng" id="lng" >
                    </div>
                </div>
            </div>
            <script>
                let map;
                let marker;

                function initMap() {
                    const myLatLng = new google.maps.LatLng(34.678260, -1.910568);
                    map = new google.maps.Map(document.getElementById('map'), {
                        center: myLatLng,
                        zoom: 15,
                        draggable: false
                    });
                    marker = new google.maps.Marker({
                        position: myLatLng,
                        map: map,
                        title: 'Ma location',
                    });
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(function (position) {
                            var pos = {
                                lat: position.coords.latitude,
                                lng: position.coords.longitude
                            };
                            map.setCenter(pos);
                            marker.setPosition(pos);
                            $('#lat').val(pos.lat);
                            $('#lng').val(pos.lng);
                            var request = new XMLHttpRequest();

                            var method = 'GET';
                            var url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + position.coords.latitude + ',' + position.coords.longitude + '&sensor=true&key=AIzaSyCm-AP_DTh0JHZN8sD30jVSH8j_33rcXVI';
                            var async = true;

                            request.open(method, url, async);
                            request.onreadystatechange = function () {
                                if (request.readyState == 4 && request.status == 200) {
                                    var data = JSON.parse(request.responseText);
                                    $('#location').val(data.results[1].formatted_address);
                                }
                            };
                            request.send();

                        });
                    }
                    google.maps.event.addListener(marker,'position_changed',function(){
                        var lat = marker.getPosition().lat();
                        var lng = marker.getPosition().lng();

                    });
                }
            </script>
            <script
                    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCm-AP_DTh0JHZN8sD30jVSH8j_33rcXVI&callback=initMap"
                    async defer>
            </script>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <button type="submit" class="btn btn-success">Créer</button>
                </div>
            </div>
        </form>
    </div>

@endsection