@extends('layouts.app')
@section('content')
    <div>
        <div class="row m-5">
            <div class="col-12 col-md-3 mb-3">
                <a class="btn btn-warning" href="{{action('QuestionController@create')}}">
                    <i class="fa fa-plus mr-4"></i> Ajouter une question
                </a>
            </div>
            <div class="d-flex justify-content-end col-12 col-md-3 offset-md-6 mb-3">
                <a class="btn btn-danger" href="{{action('QuestionController@displayFavorites')}}">
                    <i class="fa fa-heart mr-4"></i> Mes favorites
                </a>
            </div>
        </div>
        <br/>
        <div class="row mr-5 ml-5 mt-2">
            @if (\Session::has('success'))
                <div class="col-12 alert alert-success">
                    {{ \Session::get('success') }}
                </div><br/>
            @endif
            <table class="table table-striped">
                <thead>
                <tr>
                    <th width="13%">Utilisateur</th>
                    <th width="13%">Titre</th>
                    <th width="48%">Contenu</th>
                    <th width="13%">Location</th>
                    <th width="13%">Action</th>
                </tr>
                </thead>
                <tbody>

                @foreach($questions as $question)
                    <tr>
                        <td>{{$question->user->name}}</td>
                        <td>{{$question->title}}</td>
                        <td>{{$question->content}}</td>
                        <td>{{$question->location}}</td>
                        <td>
                            <a class="btn btn-outline-info  mr-1"
                               href="{{action('ResponseController@index', $question->id)}}"><i
                                        class="fa fa-eye"></i></a>
                            @php
                                $isFavorite = false;
                            @endphp
                            @foreach($userr->favorites as $favorite)
                                @if ($question->id == $favorite->id)
                                    <a class="btn btn-danger text-white ml-1" onclick="event.preventDefault();
                                            document.getElementById('{{$question->id}}').submit();"><i
                                                class="fa fa-heart"></i></a>
                                    <form id="{{$question->id}}" action="{{url('questions/removeFavoriteIndex')}}" method="POST"
                                          style="display: none;">
                                        @csrf
                                        <input name="question_id" value="{{$question->id}}">
                                    </form>
                                    @php
                                        $isFavorite = true;
                                    @endphp
                                    @break
                                @endif
                            @endforeach
                            @if (!$isFavorite)
                                <a class="btn btn-outline-danger ml-1" onclick="event.preventDefault();
                                        document.getElementById('{{$question->id}}').submit();"><i
                                            class="fa fa-heart"></i></a>
                                <form id="{{$question->id}}" action="{{url('questions/favorite')}}" method="POST"
                                      style="display: none;">
                                    @csrf
                                    <input name="question_id" value="{{$question->id}}">
                                </form>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <script>
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var request = new XMLHttpRequest();

                var method = 'PUT';
                var url = 'userLocation';
                var async = true;
                var params = "lat="+position.coords.latitude+'&lng='+position.coords.longitude;
                request.open(method, url, async);
                request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                request.send(params);
            });
        }
    </script>
@endsection