@extends('layouts.app')
@section('content')
<div>
    <div class="row m-5">
        <div class="btn btn-warning col-12 col-md-3 mb-3">
            <a class="btn btn-warning" href="{{action('QuestionController@index')}}">
                <i class="fa fa-arrow-left mr-4"></i> Retour aux questions
            </a>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="col-12 d-flex justify-content-center"><h2>{{$question->title}}</h2></div>
                </div>
                <div class="card-body pb-0">
                    <div class="col-12"><p>{{$question->content}}</p></div>
                    <p class="w-100 text-right"><sup>{{$question->location}}
                            le {{date('d / m / Y', strtotime($question->created_at)) }}</sup></p>
                </div>
            </div>
        </div>
    </div>
    <div class="row m-5">
        <p>Réponses :</p>
        @foreach($responses as $response)
            <div class="col-12">
                <div class="card mb-2">
                    <div class="card-body pb-0">
                        <p><span>{{$response->user->name}} : </span>{{$response->content}}</p>
                        <p><sup>{{date('d / m / Y', strtotime($response->created_at)) }}</sup></p>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="row m-5">
        <div class="col-12">
            <h5>Ajouter une réponse :</h5><br/>
        </div>
        <div class="col-12">
            <form method="post" action="{{url('responses/add')}}">
                @csrf
                <div class="row">
                    <div class="form-group col-md-12">
                        <textarea type="text" class="form-control" name="content" required> </textarea>
                    </div>
                </div>
                <input type="text" class="form-control" name="id" hidden value="{{$question->id}}">
                <div class="row">
                    <div class="form-group col-md-12">
                        <button type="submit" class="btn btn-success">Soumettre</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection