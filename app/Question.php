<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Elasticquent\ElasticquentTrait;

class Question extends Eloquent
{
    use ElasticquentTrait;
    protected $hidden = ['_id'];
    protected $connection = 'mongodb';
    protected $collection = 'questions';

    protected $fillable = [
        'title', 'content','location', 'geolocation'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function responses()
    {
        return $this->hasMany(Response::class);
    }
}
