<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Response extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'responses';

    protected $fillable = [
        'content'
    ];

    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
