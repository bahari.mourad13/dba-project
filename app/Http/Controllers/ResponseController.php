<?php

namespace App\Http\Controllers;

use App\Question;
use App\Response;
use Illuminate\Http\Request;
use Auth;

class ResponseController extends Controller
{
    public function index($id)
    {
        $question= Question::find($id);
        $responses= Response::where("question_id",'=',''.$id)->orderBy('created_at', 'desc')->get();
        return view('question/show',compact('responses','question'));
    }

    public function store(Request $request)
    {
        $user = Auth::user();
        $response = new Response();
        $response->question_id = $request->get('id');
        $response->content = $request->get('content');
        $response->user_id = $user->id;
        $response->save();
        return redirect()->route('detail',$request->get('id'));
    }
}
