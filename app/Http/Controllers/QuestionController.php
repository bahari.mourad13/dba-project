<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;
use Auth;

class QuestionController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $latUser = $user->lat;
        $lngUser = $user->lng;
        $questions = Question::all();
        $distances = [];
        foreach ($questions as $question) {
            $theta = $question->lng - $lngUser;
            $dist = sin(deg2rad($question->lat)) * sin(deg2rad($latUser)) + cos(deg2rad($question->lat)) * cos(deg2rad($latUser)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $distances[] = array($question, $dist);
        }

        usort($distances, function ($distanceA, $distanceB) {
            if ($distanceA[1] > $distanceB[1]) return -1;
            else if ($distanceA[1] < $distanceB[1]) return 1;
            else return 0;
        });
        /*Question::reindex();
        $sort = array(
            array(
                "_geo_distance" => array(
                    "geolocation" => array(
                        "lat" => 18.9295932,
                        "lon" => 72.83272960000001
                    ) ,
                    "order" => "asc",
                    "unit" => "km"
                )
            ),
        );
        $questions = Question::searchByQuery(null,null,null,null,null,$sort);*/

        $questions = array_column($distances, 0);
        $data = array('userr' => Auth::user(),
            'questions' => $questions);
        return view('question/index')->with($data);
    }

    public function create()
    {
        return view('question/create');
    }


    public function store(Request $request)
    {
        $user = Auth::user();
        $question = new Question();
        $question->title = $request->get('title');
        $question->content = $request->get('content');
        $question->location = $request->get('location');
        $question->user_id = $user->id;
        $question->geolocation = [
            "type" => 'geo_point',
            "fielddata" => true,
            [
                $request->get('lat'),
                $request->get('lng')
            ]
        ];
        $question->lat = $request->get('lat');
        $question->lng = $request->get('lng');
        $question->save();
        $question->addToIndex();
        return redirect('questions')->with('success', 'La question a été publiée avec succès');
    }

    public function favorite(Request $request)
    {
        $user = Auth::user();
        $question = Question::where('_id', '=', $request->get('question_id'))->first();
        $user->favorites()->associate($question);
        $user->save();
        return redirect('questions')->with('success', 'La question a été ajoutée aux favoris avec succès');
    }

    public function displayFavorites()
    {
        $user = Auth::user();
        $questionsFavoris = $user->favorites()->get();
        return view('question/favorite', compact('questionsFavoris'));
    }

    public function removeFavorite(Request $request)
    {
        $user = Auth::user();
        $question = Question::where('_id', '=', $request->get('question_id'))->first();
        $user->favorites()->dissociate($question);
        $user->save();
        return redirect('questions/favorites')->with('success', 'La question a été supprimée de votre liste de favoris');
    }

    public function removeFavoriteIndex(Request $request)
    {
        $user = Auth::user();
        $question = Question::where('_id', '=', $request->get('question_id'))->first();
        $user->favorites()->dissociate($question);
        $user->save();
        return redirect('questions')->with('success', 'La question a été supprimée de votre liste de favoris');
    }

    public function saveLoggedUserLocation(Request $request)
    {
        $user = Auth::user();
        $user->lat = floatval($request->get('lat'));
        $user->lng = floatval($request->get('lng'));
        $user->save();
    }
}