<p align="center"><img src="https://dba.ma/wp-content/uploads/2018/11/logosite.png" width="150"></p>

####digital brains agency

## DBA Project

WebApp that allows people to post questions
related to a location and get answers from other users close by, users can see
questions posted nearby their location and they can keep track of their favorite ones.

###Features:
- As a User, I can sign up using my email & password
- As a User, I can sign in using my email & password
- As a User, I can post a question with these attributes title, content, location
- As a User, I can post an answer to a question
- As a User, I can display the list of questions sorted by distance from Elasticsearch
- As a User, I can like a question, so it can be added to my favorites

###Technologies :
- Laravel
- Mongodb
- Docker
- Elasticsearch

###Run :
- Clone the repo.
- Update your IP for Elasticsearch to work in config/elasticquent.php
```
'config' => [
    'hosts'     => ['YOUR_IP:9200'],
    'retries'   => 1,
],
```
- Build docker container
```
docker-compose build
```
- Run docker containers
```
docker-compose up -d
```

[Mourad Bahari](https://www.linkedin.com/in/mourad-bahari-9192a2166/)
